import json
import os
import sqlite3
import sys
import urllib.parse
import uuid

import base62
from flask import Flask, jsonify, request, Response, g, render_template


app = Flask(__name__)


DATABASE = os.environ.get('SHORTENER_DB', None)
if DATABASE is None:
    print("SHORTENER_DB envvar not specified")
    sys.exit(1)


URL_PREFIX = os.environ.get('SHORTENER_URL_PREFIX', None)


DDL = """
CREATE TABLE IF NOT EXISTS shortener(
    key TEXT PRIMARY_KEY,
    content TEXT NOT NULL UNIQUE,
    dt_created TIMESTAMP NOT NULL,
    dt_last_accessed TIMESTAMP,
    hits INT NOT NULL);
"""


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.execute(DDL)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def key_set(key: str, content: str):
    with get_db() as conn:
        try:
            conn.execute(
                    "INSERT INTO shortener(key, content, dt_created, dt_last_accessed, hits) VALUES (?, ?, DATETIME('now'), null, 0)",
                    (key, content))
            return key
        except sqlite3.IntegrityError as e:
            res = conn.execute("SELECT key FROM shortener WHERE content=?", (content, )).fetchone()
            if res is None:
                return False
            return res[0]


def key_get(key: str, with_data_update: bool):
    try:
        with get_db() as conn:
            res = conn.execute("SELECT key, content FROM shortener WHERE key=?", (key, )).fetchone()
            if res is None:
                return None

            if with_data_update:
                conn.execute("UPDATE shortener SET dt_last_accessed = DATETIME('NOW'), hits=hits + 1 WHERE key=?", (key, ))

            return res[1]
    except sqlite3.Error as e:
        print(e)
        return None


def key_dump():
    try:
        with get_db() as conn:
            return conn.execute("SELECT key, dt_created, dt_last_accessed, hits FROM shortener").fetchall()
    except sqlite3.Error as e:
        print(e)
        return None



def is_json(s: str):
    try:
        json.loads(s)
    except Exception as e:
        return False
    return True


@app.route('/create', methods=["POST"])
def create():
    content = request.get_data()
    if not is_json(content):
        return jsonify({"error": "Not a valid JSON."}), 500

    for _ in range(20):
        uid = uuid.uuid4()

        key = key_set(str(uid), content)
        if key is None:
            continue
        encoded_key = base62.encode(uuid.UUID(key).int)
        resp = {'key': encoded_key}
        if URL_PREFIX:
            resp['url'] = urllib.parse.urljoin(URL_PREFIX, encoded_key)
        return jsonify(resp)

    return jsonify({"error": "Couldn't generate unique ID for your request, please try again"}), 500


@app.route('/get/<string:key>')
def get(key: str):
    try:
        decoded_key = base62.decode(key)
        db_key = str(uuid.UUID(int=decoded_key))
    except ValueError:
        # allow querying using UUID key as well
        db_key = key
    except TypeError:
        return jsonify({"error": "Not found", "key": key}), 404

    with_data_update = 'nostats' not in request.args
    content = key_get(db_key, with_data_update)
    if content is None:
        return jsonify({"error": "Not found", "key": key}), 404

    mimetype = 'application/json' if is_json(content) else 'application/text'
    return Response(content, mimetype=mimetype, status=200)


@app.route('/list/api')
def listapi():
    data = [{'key': base62.encode(uuid.UUID(r[0]).int),
             'db_key': r[0],
             'dt_created': r[1],
             'dt_last_accessed': r[2],
             'hits': r[3]} for r in key_dump()]
    return jsonify(data)

@app.route('/list')
def list():
    return render_template('list.html')

if __name__ == '__main__':
    app.run(host='localhost', port=3002, debug=True)
