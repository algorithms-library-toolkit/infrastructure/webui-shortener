FROM alpine:3.16 as shortener

COPY app.py /app/app.py
COPY templates /app/templates
COPY requirements.txt /app/requirements.txt

RUN apk add --no-cache py3-pip py3-gunicorn && pip install -r /app/requirements.txt && rm /app/requirements.txt

EXPOSE 80
CMD ["gunicorn", "-b", "0.0.0.0:80", "-w", "4", "--chdir", "/app", "app:app"]
