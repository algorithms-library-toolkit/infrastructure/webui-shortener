# Algorithms Library Toolkit - WebUI shortener server

This simple flask server serves as a backend server for URL shortener in the webui.
It runs Redis DB and flask HTTP server and it just stores and retrieves stores JSONs (or any other strings) under an UUID.

## Requirements

- redis-server (running on localhost:6379, passwordless)
- redis-py (pip install redis)
 flask (pip install flask)

